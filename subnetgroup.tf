resource "aws_db_subnet_group" "mydbgroup" {
  name        = "mydbgroup"
  description = "My DB subnet group"
  subnet_ids  = ["subnet-0a0f36a9d0e1286b8", "subnet-05fe2645f245ceeba"]

}
