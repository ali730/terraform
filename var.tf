variable "database_user" {
  description = "admin"
}

variable "database_password" {
  description = "abcd1234"
}

variable "database_name" {
  description = "procoredb"
}

# Create rds DB
resource "aws_db_instance" "wordpressdb" {
  allocated_storage    = 20
  engine               = "mariadb"
  engine_version       = "10.6.14"
  instance_class       = "db.t3.micro"
  storage_type         = "gp2"
  username             = "admin"
  password             = "abcd1234"
  db_subnet_group_name = aws_db_subnet_group.mydbgroup.name
  publicly_accessible  = false
  skip_final_snapshot  = true

  vpc_security_group_ids = ["sg-05aa75971a8e9252e"]
}

// -----------------------------------------------
// Change USERDATA variable value after grabbing RDS endpoint info
// -----------------------------------------------
data "template_file" "user_data" {
  template = file("userdata.sh")
  vars = {
    db_username      = var.database_user
    db_user_password = var.database_password
    db_name          = var.database_name
    db_RDS           = aws_db_instance.wordpressdb.endpoint
  }
}

