resource "aws_instance" "lamp_instance" {
  ami           = data.aws_ami.amzLinux.id
  instance_type = "t2.micro"
  key_name      = "MyKeyPair"
  subnet_id     = "subnet-052961f1e8db0533b"
  user_data     = data.template_file.user_data.rendered
  tenancy       = "default"

  vpc_security_group_ids = ["sg-034e3754d9183e021"]
}